# README #



1.Create a terraform module that creates the following resources:
      1.1 An S3 bucket with private default acl
      1.2 A redshift cluster with IAM role that allows read/write access to the bucket
2.Input parameters:
      2.1 Number of nodes
      2.2 Node type/size
      2.3 Name of bucket/cluster
      2.4 Charge Code tag to be applied to all resources
      2.5 Any other parameters you think are required
3.Outputs:
      3.1 Redshift endpoint
      3.2 S3 bucket ARN
      3.3 Any other information you think would be useful to have as an output