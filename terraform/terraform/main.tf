#s3 bucket creation

resource "aws_s3_bucket" "main" {
  bucket = "${var.name}"
  acl    = "${var.acl}"

  versioning {
    enabled = "${var.enable_versioning}"
  }

  tags = "${var.tags}"

  #   lifecycle = "${var.lifecycle}"
}

# redshift cluster creation

resource "aws_redshift_cluster" "main_redshift_cluster" {
  cluster_identifier = "${var.cluster_identifier}"
  cluster_version    = "${var.cluster_version}"
  node_type          = "${var.cluster_node_type}"
  number_of_nodes    = "${var.cluster_number_of_nodes}"
  database_name      = "${var.cluster_database_name}"
  master_username    = "${var.cluster_master_username}"
  master_password    = "${var.cluster_master_password}"

  port = "${var.cluster_port}"

  # Because we're assuming a VPC, we use this option, but only one SG id
  vpc_security_group_ids = ["${aws_security_group.main_redshift_access.id}"]

  # We're creating a subnet group in the module and passing in the name
  cluster_subnet_group_name    = "${aws_redshift_subnet_group.main_redshift_subnet_group.name}"
  cluster_parameter_group_name = "${aws_redshift_parameter_group.main_redshift_cluster.id}"

  publicly_accessible = "${var.publicly_accessible}"

  # Snapshots and backups
  skip_final_snapshot                 = "${var.skip_final_snapshot}"
  automated_snapshot_retention_period = "${var.automated_snapshot_retention_period }"
  preferred_maintenance_window        = "${var.preferred_maintenance_window}"

  # IAM Roles
  iam_roles = ["${var.cluster_iam_roles}"]

  lifecycle {
    prevent_destroy = true
  }
}
