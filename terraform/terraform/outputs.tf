# Output the ID of the Redshift cluster

output "redshift_cluster_id" {
  value = "${aws_redshift_cluster.main_redshift_cluster.id}"
}

# Output address (hostname) of the Redshift cluster
output "redshift_cluster_address" {
  value = "${replace(aws_redshift_cluster.main_redshift_cluster.endpoint, format(":%s", aws_redshift_cluster.main_redshift_cluster.port), "")}"
}

# Output endpoint (hostname:port) of the Redshift cluster
output "redshift_cluster_endpoint" {
  value = "${aws_redshift_cluster.main_redshift_cluster.endpoint}"
}